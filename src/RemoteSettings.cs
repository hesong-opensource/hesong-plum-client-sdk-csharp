using System.Collections.Generic;

using Newtonsoft.Json;

namespace hesong.plum.client
{
    public class RemoteSettings
    {
        [JsonProperty("workspace")]
        public Dictionary<string, string> Workspace { get; set; }
    }
}
