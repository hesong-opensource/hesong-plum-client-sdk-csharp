using Newtonsoft.Json;

namespace hesong.plum.client
{
    public class AcdOptions
    {
        [JsonProperty("connect_timeout")]
        public int ConnectTimeout { get; set; }

        [JsonProperty("dial_timeout")]
        public int DialTimeout { get; set; }

        [JsonProperty("distribute_timeout")]
        public int DistributeTimeout { get; set; }

        [JsonProperty("talk_timeout")]
        public int TalkTimeout { get; set; }

        [JsonProperty("call_from")]
        public string CallFrom { get; set; }

        [JsonProperty("call_agent_from")]
        public string CallAgentFrom { get; set; }
    }
}