using Newtonsoft.Json;

namespace hesong.plum.client
{
    public class AgentInfo
    {
        [JsonProperty("name")]
        public string Name{ get; set; }

        [JsonProperty("telnum")]
        public string Telnum { get; set; }
    }
}