using System.Collections.Generic;

using Newtonsoft.Json;

namespace hesong.plum.client
{
    public class TalkerInfo
    {
        [JsonProperty("agent")]
        public AgentInfo Agent { get; set; }

        [JsonProperty("call")]
        public CallInfo Call { get; set; }

        [JsonProperty("attr")]
        public Dictionary<string, object> Attr { get; set; }
    }
}
