namespace hesong.plum.client
{
    public enum AcdState
    {
        Disposed = -1,
        Initial = 0,
        Distributing = 1,
        DIstributed = 2,
        InitialAgentConnecting = 3,
        InitialAgentConnected = 4,
        TargetAgentConnecting = 5,
        TargetLineConnecting = 6,
        Talking = 7
    }
}