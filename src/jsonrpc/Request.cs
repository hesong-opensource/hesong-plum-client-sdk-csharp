using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace hesong.plum.client.jsonrpc
{
    class Request
    {
        public Request(string id, string method, IDictionary<string, JToken> parameters)
        {
            this.id = id;

            if (string.IsNullOrEmpty(method))
            {
                throw new ArgumentNullException("method");
            }
            this.method = method ;
            this.parameters = parameters ?? new Dictionary<string, JToken>();
        }

        private string id;
        public string Id { get { return id; } }

        private string method;
        public string Method { get { return method; } }

        private IDictionary<string, JToken> parameters;
        public IDictionary<string, JToken> Parameters { get { return parameters; } }
    }
}
