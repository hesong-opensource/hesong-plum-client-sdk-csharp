namespace hesong.plum.agent.client.jsonrpc
{
    interface ISender
    {
        void Send(string msg);
    }
}