namespace hesong.plum.client.jsonrpc
{
    enum ResponseState
    {
        Pending,
        Result,
        Error
    }
}