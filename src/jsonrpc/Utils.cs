using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace hesong.plum.client.jsonrpc
{
    class Utils
    {
        public static string JSONRPC = "2.0";

        public static JObject MakeRequestJson(string id, string method, JObject parameters)
        {
            method = method.Trim();
            if (string.IsNullOrEmpty(method))
            {
                throw new ArgumentNullException("method");
            }
           
            if (parameters == null)
                parameters = new JObject();
            var json = new JObject(
                new JProperty("jsonrpc", JSONRPC),
                new JProperty("method", method),
                new JProperty("params", parameters)
            );
            if (id != null)
                json["id"] = id;
            return json;
        }

        public static JObject MakeRequestJson(string id, string method, IDictionary<string, object> parameters)
        {
            JObject json = null;
            if (parameters != null)
            {
                var s = JsonConvert.SerializeObject(parameters);
                json = JObject.Parse(s);
            }
            return MakeRequestJson(id, method, json);
        }

        public static JObject MakeResponseJson(string id, JToken result)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentException("`id` can not be null or white space", "id");
            return new JObject(
                new JProperty("jsonrpc", JSONRPC),
                new JProperty("id", id),
                new JProperty("result", result)
            );
        }

        public static JObject MakeResponseJson(string id, object result)
        {
            var s = JsonConvert.SerializeObject(result);
            var json = JToken.Parse(s);
            return MakeResponseJson(id, json);
        }
    }
}