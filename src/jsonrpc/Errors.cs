using System;

namespace hesong.plum.client.jsonrpc
{
    public class Error : Exception
    {
        public Error(string id, int code, string message)
            : base(message)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            this.id = id;
            this.code = code;
        }

        public Error(string id, int code)
            : base()
        {
            this.id = id;
            this.code = code;
        }

        private string id;
        public String Id
        {
            get { return id; }
        }

        private int code;
        public int Code
        {
            get { return code; }
        }
    }
}
