using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

namespace hesong.plum.client.jsonrpc
{
    class Manager
    {
        public Manager()
        {
        }

        /// <summary>
        /// {id: (state, result, error)} 
        /// </summary>
        private IDictionary<string, Tuple<ResponseState, JToken, Error>> rpcDict =
            new Dictionary<string, Tuple<ResponseState, JToken, Error>>();

        private JToken WaitResponse(string id, CancellationToken cancelToken)
        {
            while (true)
            {
                if (cancelToken.IsCancellationRequested)
                    cancelToken.ThrowIfCancellationRequested();
                Tuple<ResponseState, JToken, Error> response;
                lock (rpcDict)
                {
                    if (rpcDict.TryGetValue(id, out response))
                    {
                        if (response.Item1 != ResponseState.Pending)
                            rpcDict.Remove(id);
                    }
                    else
                    {
                        throw new KeyNotFoundException(string.Format("{0}", id));
                    }
                }
                switch (response.Item1)
                {
                    case ResponseState.Result:
                        return response.Item2;
                    case ResponseState.Error:
                        throw response.Item3;
                }
                Thread.Sleep(10);
            }
        }

        public JToken RemoteCall(Agent agent, string method)
        {
            return RemoteCall(agent, method, null, 5000);
        }

        public JToken RemoteCall(Agent agent, string method, JObject parameters)
        {
            return RemoteCall(agent, method, parameters, 5000);
        }

        public JToken RemoteCall(Agent agent, string method, JObject parameters, int millisecondsTimeout)
        {
            var id = Guid.NewGuid().ToString();
            var req = Utils.MakeRequestJson(id, method, parameters);
            var msg = req.ToString();
            // Save to map before
            lock (rpcDict)
            {
                rpcDict[id] = new Tuple<ResponseState, JToken, Error>(
                    ResponseState.Pending, null, null
                );
            }
            try
            {
                // Send
                agent.Send(msg);
                // Response waiting Task
                var cancelSource = new CancellationTokenSource();
                try
                {
                    var t = new Task<JToken>(() =>
                    {
                        return WaitResponse(id, cancelSource.Token);
                    }, cancelSource.Token);
                    t.Start();
                    try
                    {
                        if (t.Wait(millisecondsTimeout))
                        {
                            return t.Result;
                        }
                        else
                        {
                            cancelSource.Cancel();
                            throw new TimeoutException();
                        }
                    }
                    catch (AggregateException ae)
                    {
                        foreach (var e in ae.InnerExceptions)
                            throw e;
                        throw;
                    }
                }
                finally
                {
                    cancelSource.Dispose();
                }
            }
            finally
            {
                lock (rpcDict)
                {
                    rpcDict.Remove(id);
                }
            }
        }

        public Request ProcessReceivedMessage(Agent agent, string msg)
        {
            IDictionary<string, JToken> json = JObject.Parse(msg);
            var id = (string)json["id"];
            Tuple<ResponseState, JToken, Error> response = null;
            if (json.ContainsKey("method"))
            {
                return new Request(id, (string)json["method"], json["params"].ToObject<IDictionary<string, JToken>>());
            }
            else if (json.ContainsKey("result"))
            {
                if (string.IsNullOrEmpty(id))
                    return null;
                lock (rpcDict)
                {
                    if (rpcDict.TryGetValue(id, out response))
                    {
                        if (response.Item1 != ResponseState.Pending)
                            throw new ThreadStateException("response state error.");
                        rpcDict[id] = new Tuple<ResponseState, JToken, Error>(
                            ResponseState.Result,
                            json["result"],
                            null
                        );
                    }
                }
            }
            else if (json.ContainsKey("error"))
            {
                if (id == null)
                    return null;
                lock (rpcDict)
                {
                    if (rpcDict.TryGetValue(id, out response))
                    {
                        if (response.Item1 != ResponseState.Pending)
                            throw new ThreadStateException("response state error.");
                        rpcDict[id] = new Tuple<ResponseState, JToken, Error>(
                            ResponseState.Error,
                            null,
                            new Error(id, (int)json["error"]["code"], (string)json["error"]["message"])
                        );
                    }
                }
            }
            return null;
        }
    }
}