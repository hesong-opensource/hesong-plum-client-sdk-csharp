using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;
using WebSocketSharp;

namespace hesong.plum.client
{
    public class Agent
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Object thisLock = new Object();
        public Agent(string urlPrefix, string name)
        {
            if (string.IsNullOrEmpty(urlPrefix))
            {
                throw new ArgumentNullException("urlPrefix");
            }
            this.urlPrefix = urlPrefix;
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
            log.InfoFormat("new Agent(\"{0}\", \"{1}\")", urlPrefix, name);
            this.name = name;
            this.urlPrefix = urlPrefix = urlPrefix.Trim().TrimEnd(new char[] { '/' });
            connected = false;
            manager = new jsonrpc.Manager();
        }

        private jsonrpc.Manager manager;

        #region WebSocket
        private WebSocket ws;

        private void Ws_OnOpen(object sender, EventArgs e)
        {
            log.Info("Ws_OnOpen");
            lock (thisLock)
            {
                state = AgentState.Online;
                connected = true;
            }
            new Thread(() =>
            {
                OnConnect?.Invoke(this, e);
            }).Start();
        }

        private void Ws_OnClose(object sender, CloseEventArgs e)
        {
            log.WarnFormat("Ws_OnClose code={0}, reason={1}", e.Code, e.Reason);
            lock (thisLock)
            {
                connected = false;
            }
            if (ws != null && !ws.IsAlive)
            {
                if (e.Code >= 4000 && e.Code < 5000)
                {
                    new Thread(() =>
                    {
                        OnLogout?.Invoke(this, new LogoutEventArgs(e.Code, e.Reason));
                    }).Start();
                }
                else
                {
                    new Thread(() =>
                    {
                        OnDisconnect?.Invoke(this, e);
                    }).Start();
                }

                ws = null;
            }

        }

        private void Ws_OnError(object sender, ErrorEventArgs e)
        {
            log.ErrorFormat(string.Format("Ws_OnError: {0}", e.Message));
            if (ws != null && !ws.IsAlive)
            {
                //connected = false;
                //new Thread(() =>
                //{
                //    OnDisconnect?.Invoke(this, e);
                //}).Start();
                //ws = null;
            }
        }

        private void Ws_OnMessage(object sender, MessageEventArgs e)
        {
            log.DebugFormat("Ws_OnMessage: {0}", e.Data);
            var req = manager.ProcessReceivedMessage(this, e.Data);
            if (req == null)
                return;
            /// 对于事件通知，一律返回 `null`，让服务器知道客户端已经收到即可！
            if (!string.IsNullOrEmpty(req.Id))
            {
                var response = jsonrpc.Utils.MakeResponseJson(req.Id, null);
                Send(response.ToString());
            }

            var task = new Task(() =>
            {
                switch (req.Method)
                {
                    case "api.agent.on_message_triggered":
                        {
                            string message = (string)req.Parameters["message"];
                            MessageTriggeredEvent(new MessageTriggeredEventArgs(message));
                        }
                        break;

                    case "api.agent.on_state_changed":
                        {
                            JToken jsAgent = req.Parameters["agent"];
                            AgentState priorState = req.Parameters["prior_state"].ToObject<AgentState>();
                            SetAgent(jsAgent);
                            StateChangedEvent(new StateChangedEventArgs(priorState));
                        }
                        break;

                    case "api.agent.on_acd_state_changed":
                        {
                            JToken jsAgent = req.Parameters["agent"];
                            AcdInfo acdInfo = req.Parameters["acd"].ToObject<AcdInfo>();
                            AcdState? priorAcdState = req.Parameters["prior_state"].ToObject<AcdState?>();
                            string errMsg = Convert.ToString(req.Parameters["error"]);
                            SetAgent(jsAgent);
                            AcdStateChangedEvent(new AcdStateChangedEventArgs(acdInfo, priorAcdState, errMsg));
                        }
                        break;
                    case "api.agent.on_acd_talker_changed":
                        {
                            JToken jsAgent = req.Parameters["agent"];
                            AcdInfo acdInfo = req.Parameters["acd"].ToObject<AcdInfo>();
                            TalkerInfo talkerInfo = req.Parameters["talker"].ToObject<TalkerInfo>();
                            int mode = Convert.ToInt32(req.Parameters["mode"]);
                            SetAgent(jsAgent);
                            AcdTalkerChangedEvent(new AcdTalkerChangedEventArgs(acdInfo, talkerInfo, mode));
                        }
                        break;
                }
            });
            task.ContinueWith(t =>
            {
                t.Exception.Handle(excp =>
                {
                    return false;
                });
            }, TaskContinuationOptions.OnlyOnFaulted);
            task.Start();
        }

        internal void Send(string msg)
        {
            log.DebugFormat("ws_Send: {0}", msg);
            ws.Send(msg);
        }

        #endregion

        private string urlPrefix;
        public string UrlPrefix => urlPrefix;
        private string name;
        public string Name => name;
        private string token;
        public string Token => token;

        #region Connect

        private bool connected;
        public bool Connected => connected;

        public void Connect()
        {
            if (ws != null)
                return;
            var uri = new Uri(urlPrefix);
            var schema = "ws";
            if (uri.Scheme == "https")
                schema = "wss";
            var host = uri.Host;
            var port = uri.Port;
            var url = string.Format(
                "{0}://{1}:{2}/api/agent/{3}/connect?token={4}",
                schema, host, port, Uri.EscapeDataString(name), Uri.EscapeDataString(token)
            );
            ws = new WebSocket(url);
            ws.OnOpen += Ws_OnOpen;
            ws.OnClose += Ws_OnClose;
            ws.OnMessage += Ws_OnMessage;
            ws.OnError += Ws_OnError;
            ws.Connect();
        }
        public void Disconnect()
        {
            ws.Close();
            ws = null;
        }

        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;

        #endregion

        #region AgentState

        private AgentState state = AgentState.None;
        public AgentState State { get => state; set { SetState(value); } }
        public void SetState(AgentState value)
        {
            var parameters = new JObject(
                new JProperty("state", (long)value)
            );
            manager.RemoteCall(this, RPC_NS + "set_state", parameters);
            state = value;
        }

        public class StateChangedEventArgs : EventArgs
        {
            public StateChangedEventArgs(AgentState priorState) => this.priorState = priorState;

            private readonly AgentState priorState;
            public AgentState PriorState => priorState;
        }

        public delegate void StateChangedEventHandler(object sender, StateChangedEventArgs e);
        public event StateChangedEventHandler OnStateChanged;
        protected virtual void StateChangedEvent(StateChangedEventArgs e)
        {
            OnStateChanged?.Invoke(this, e);
        }
        #endregion

        #region LogoutEvent
        public class LogoutEventArgs : EventArgs
        {
            public LogoutEventArgs(int code, string reason)
            {
                this.code = code;
                this.reason = reason;
            }

            private readonly int code;
            public int Code => code;
            private readonly string reason;
            public string Reason => reason;
        }

        public delegate void LogoutEventHandler(object sender, LogoutEventArgs e);
        public event LogoutEventHandler OnLogout;
        protected virtual void LogoutEvent(LogoutEventArgs e)
        {
            OnLogout?.Invoke(this, e);
        }
        #endregion

        #region AcdStateChangedEvent
        public class AcdStateChangedEventArgs : EventArgs
        {
            public AcdStateChangedEventArgs(AcdInfo acdInfo, AcdState? priorState, string errorMessage)
            {
                this.acdInfo = acdInfo ?? throw new ArgumentNullException(nameof(acdInfo));
                this.priorState = priorState;
                this.errorMessage = errorMessage;
            }

            private readonly string errorMessage;
            public string ErrorMessage => errorMessage;

            private readonly AcdInfo acdInfo;
            public AcdInfo AcdInfo => acdInfo;

            private readonly AcdState? priorState;
            public AcdState? PriorState => priorState;
        }

        public delegate void AcdStateChangedEventHandler(object sender, AcdStateChangedEventArgs e);
        public event AcdStateChangedEventHandler OnAcdStateChanged;
        protected virtual void AcdStateChangedEvent(AcdStateChangedEventArgs e)
        {
            OnAcdStateChanged?.Invoke(this, e);
        }

        #endregion

        #region AcdTalkerChangedEvent
        public class AcdTalkerChangedEventArgs : EventArgs
        {
            public AcdTalkerChangedEventArgs(AcdInfo acdInfo, TalkerInfo talkerInfo, int mode)
            {
                this.acdInfo = acdInfo ?? throw new ArgumentNullException(nameof(acdInfo));
                this.talkerInfo = talkerInfo ?? throw new ArgumentNullException(nameof(talkerInfo));
                this.mode = mode;
            }

            private readonly AcdInfo acdInfo;
            public AcdInfo AcdInfo => acdInfo;

            private readonly TalkerInfo talkerInfo;
            public TalkerInfo TalkerInfo => talkerInfo;

            private readonly int mode;
            public int Mode => mode;
        }

        public delegate void AcdTalkerChangedEventHandler(object sender, AcdTalkerChangedEventArgs e);
        public event AcdTalkerChangedEventHandler OnAcdTalkerChanged;
        protected virtual void AcdTalkerChangedEvent(AcdTalkerChangedEventArgs e)
        {
            OnAcdTalkerChanged?.Invoke(this, e);
        }
        #endregion

        private string displayName = string.Empty;
        public string DisplayName => displayName;

        private RemoteSettings remoteSettings;
        public RemoteSettings RemoteSettings { get => remoteSettings; }

        private List<AcdInfo> acdList = new List<AcdInfo>();
        public IList<AcdInfo> AcdList => acdList;

        private List<SkillInfo> skills = new List<SkillInfo>();
        public IList<SkillInfo> Skills => skills;

        private CallAgentState? callState = null;
        public CallAgentState? CallAgentState => callState;

        private static string RPC_NS = "api.agent.";

        internal void SetAgent(JToken data)
        {
            lock (thisLock)
            {
                token = (string)data["token"];
                state = (AgentState)(int)data["state"];
                callState = (CallAgentState?)(int?)data["call_state"];
                displayName = (string)data["dn"];
                skills = data["skills"].ToObject<List<SkillInfo>>();
                acdList = data["acd_list"].ToObject<List<AcdInfo>>();
                acdList.RemoveAll(m => m.State == AcdState.Disposed); // 删除 disposed 的 ACD
            }
        }

        internal void SetRemoteSettings(JToken data)
        {
            remoteSettings = data.ToObject<RemoteSettings>();
        }

        public void Login(string password)
        {
            log.Info("Login() ...");
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("password");
            }
            var url = string.Format("{0}/api/agent/login", urlPrefix);
            var data = new JObject(
                new JProperty("name", name),
                new JProperty("password", password)
            );
            var webclient = new WebClient();
            webclient.Headers[HttpRequestHeader.ContentType] = "application/json;charset=utf-8";
            webclient.Headers[HttpRequestHeader.Accept] = "application/json";
            webclient.Headers[HttpRequestHeader.AcceptCharset] = "utf-8";
            string resTxt = webclient.UploadString(url, data.ToString());
            var resJToken = JToken.Parse(resTxt);
            SetRemoteSettings(resJToken["settings"]);
            SetAgent(resJToken["agent"]);
            log.Info("Login() Ok.");
        }

        public void Logout()
        {
            log.Info("Logout() ...");
            var url = string.Format(
                "{0}/api/agent/{1}/logout?token={2}",
                urlPrefix, Uri.EscapeDataString(name), Uri.EscapeDataString(token)
            );
            var webclient = new WebClient();
            webclient.Headers[HttpRequestHeader.ContentType] = "application/json;charset=utf-8";
            webclient.Headers[HttpRequestHeader.Accept] = "application/json";
            webclient.Headers[HttpRequestHeader.AcceptCharset] = "utf-8";
            webclient.DownloadData(url);
            log.Info("Logout() Ok.");
        }

        internal JToken Get()
        {
            return manager.RemoteCall(this, RPC_NS + "get");
        }

        #region 调试用方法/事件
        public string Echo(string txt)
        {
            var parameters = new JObject(
                new JProperty("txt", txt)
                );
            return (string)manager.RemoteCall(this, RPC_NS + "echo", parameters);
        }

        public string DelayEcho(string txt, float delay)
        {
            var parameters = new JObject(
                new JProperty("txt", txt),
                new JProperty("delay", delay)
            );
            return (string)manager.RemoteCall(this, RPC_NS + "delay_echo", parameters);
        }

        public void TriggerMessage(string txt, float delay)
        {
            var parameters = new JObject(
                new JProperty("txt", txt),
                new JProperty("delay", delay)
            );
            manager.RemoteCall(this, RPC_NS + "trigger_message", parameters);
        }

        public void Error(string message)
        {
            var parameters = new JObject(
                new JProperty("message", message)
            );
            manager.RemoteCall(this, RPC_NS + "error", parameters);
        }

        public class MessageTriggeredEventArgs : EventArgs
        {
            public MessageTriggeredEventArgs(string message) => this.message = message;

            private string message;
            public string Message => message;
        }
        public delegate void MessageTriggeredEventHandler(object sender, MessageTriggeredEventArgs e);
        public event MessageTriggeredEventHandler OnMessageTriggered;
        protected virtual void MessageTriggeredEvent(MessageTriggeredEventArgs e)
        {
            OnMessageTriggered?.Invoke(this, e);
        }
        #endregion

        # region 签入签出
        public void CheckInSkill()
        {
            CheckAllSkills(true);
        }

        public void CheckInSkill(string name)
        {
            CheckInSkill(new List<string>() { name });
        }

        public void CheckInSkill(IList<string> names)
        {
            List<bool> checks = new List<bool>();
            for (int i = 0; i < names.Count; i++)
                checks.Add(true);
            CheckSkills(names, checks);
        }

        public void CheckOutSkill()
        {
            CheckAllSkills(false);
        }

        public void CheckOutSkill(string name)
        {
            CheckOutSkill(new List<string>() { name });
        }

        public void CheckOutSkill(IList<string> names)
        {
            List<bool> checks = new List<bool>();
            for (int i = 0; i < names.Count; i++)
                checks.Add(false);
            CheckSkills(names, checks);
        }

        public void CheckSkills(IList<string> names, IList<bool> checks)
        {
            if (names.Count != checks.Count)
                throw new ArgumentException();
            var parameters = new JObject(
                new JProperty("names", names),
                new JProperty("checks", checks)
            );
            manager.RemoteCall(this, RPC_NS + "set_skill_checking", parameters);
        }

        public void CheckAllSkills(bool check)
        {
            var parameters = new JObject(
                new JProperty("names", null),
                new JProperty("checks", check)
            );
            manager.RemoteCall(this, RPC_NS + "set_skill_checking", parameters);
        }
        #endregion

        public void CallOut(string toNumber)
        {
            if (string.IsNullOrWhiteSpace(toNumber))
                throw new ArgumentException(string.Format("{0} is null or whitespace", nameof(toNumber)), nameof(toNumber));

            var parameters = new JObject(
                new JProperty("telnum", toNumber)
            );
            manager.RemoteCall(this, RPC_NS + "call_line", parameters);
        }

        public void ForwardToAgent(string agentName)
        {
            var parameters = new JObject(
                new JProperty("requires", agentName)
            );
            manager.RemoteCall(this, RPC_NS + "forward_agent", parameters);
        }

        public void ForwardToSkill(string skillName)
        {
            var parameters = new JObject(
                new JProperty("requires", new JArray(new string[1] { skillName }))
            );
            manager.RemoteCall(this, RPC_NS + "forward_agent", parameters);
        }

        public void ForwardToLine(string telnum)
        {
            if (string.IsNullOrWhiteSpace(telnum))
                throw new ArgumentNullException("telnum");

            var parameters = new JObject(
                new JProperty("telnum", telnum)
            );
            manager.RemoteCall(this, RPC_NS + "forward_line", parameters);
        }

        public void TransferToAgent(string agentName)
        {
            string acdId = null;
            if (string.IsNullOrWhiteSpace(acdId))
            {
                foreach (var acdInfo in acdList)
                {
                    foreach (var talkInfo in acdInfo.Talkers)
                    {
                        if (talkInfo.Agent != null)
                        {
                            if (talkInfo.Agent.Name == name)
                            {
                                if (Convert.ToInt32(talkInfo.Attr["voice_mode"]) == 1)
                                {
                                    acdId = acdInfo.Id;
                                    break;
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(acdId))
                        break;
                }
                if (string.IsNullOrWhiteSpace(acdId))
                    throw new Exception("没有活动的ACD交谈");
                TransferToAgent(acdId, agentName);
            }
        }

        public void TransferToAgent(string acdId, string agentName)
        {
            if (string.IsNullOrWhiteSpace(acdId))
                throw new ArgumentNullException("acdId");
            var parameters = new JObject(
                new JProperty("acd_fiid", acdId),
                new JProperty("requires", agentName)
            );
            manager.RemoteCall(this, RPC_NS + "transfer_agent", parameters);
        }

        public void TransferToSkill(string skillName)
        {
            string acdId = null;
            if (string.IsNullOrWhiteSpace(acdId))
            {
                foreach (var acdInfo in acdList)
                {
                    foreach (var talkInfo in acdInfo.Talkers)
                    {
                        if (talkInfo.Agent != null)
                        {
                            if (talkInfo.Agent.Name == name)
                            {
                                if (Convert.ToInt32(talkInfo.Attr["voice_mode"]) == 1)
                                {
                                    acdId = acdInfo.Id;
                                    break;
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(acdId))
                        break;
                }
                if (string.IsNullOrWhiteSpace(acdId))
                    throw new Exception("没有活动的ACD交谈");
                TransferToAgent(acdId, skillName);
            }
        }

        public void TransferToSkill(string acdId, string skillName)
        {
            if (string.IsNullOrWhiteSpace(acdId))
                throw new ArgumentNullException("acdId");
            var parameters = new JObject(
                new JProperty("acd_fiid", acdId),
                new JProperty("requires", new JArray(new string[1] { skillName }))
            );
            manager.RemoteCall(this, RPC_NS + "transfer_agent", parameters);
        }

        public void TransferToLine(string acdId, string telnum)
        {
            if (string.IsNullOrWhiteSpace(acdId))
                throw new ArgumentNullException("acdId");
            var parameters = new JObject(
                new JProperty("acd_fiid", acdId),
                new JProperty("telnum", telnum)
            );
            manager.RemoteCall(this, RPC_NS + "transfer_line", parameters);
        }

        public void Merge(string srcAcdId, string dstAcdId)
        {
            if (string.IsNullOrWhiteSpace(srcAcdId))
                throw new ArgumentNullException("srcAcdId");
            if (string.IsNullOrWhiteSpace(dstAcdId))
                throw new ArgumentNullException("dstAcdId");
            var parameters = new JObject(
                new JProperty("src_acd_fiid", srcAcdId),
                new JProperty("dst_acd_fiid", dstAcdId)
            );
            manager.RemoteCall(this, RPC_NS + "merge", parameters);
        }

        public void Dismiss(string acdId, string data)
        {
            if (string.IsNullOrWhiteSpace(acdId))
                throw new ArgumentNullException("acdId");
            var parameters = new JObject(
                new JProperty("acd_fiid", acdId),
                new JProperty("data", data)
            );
            manager.RemoteCall(this, RPC_NS + "dismiss", parameters);
        }

        public void JoinTalk(string acdId, TalkVoiceMode mode)
        {
            var parameters = new JObject(
                new JProperty("acd_fiid", acdId),
                new JProperty("attrs", new JObject(
                    new JProperty("voice_mode", (int)mode)
                ))
            );
            manager.RemoteCall(this, RPC_NS + "enter_talk", parameters);
        }

        public void Hold()
        {
            var parameters = new JObject();
            manager.RemoteCall(this, RPC_NS + "hold");
        }

        public void UnHold(string acdId)
        {
            var parameters = new JObject(
                new JProperty("acd_fiid", acdId)
            );
            manager.RemoteCall(this, RPC_NS + "unhold", parameters);
        }

        public void UnHold()
        {
            UnHold(null);
        }

        public void TerminateTalk(string acdId, string message)
        {
            var parameters = new JObject(
                new JProperty("acd_fiid", acdId),
                new JProperty("data", message)
            );
            manager.RemoteCall(this, RPC_NS + "terminate_talk", parameters);
        }

    }
}
