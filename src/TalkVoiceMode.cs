using System;

namespace hesong.plum.client
{
    public class TalkVoiceMode
    {
        public TalkVoiceMode(bool enableListen, bool enableSpeak)
        {
            EnableListen = enableListen;
            EnableSpeak = enableSpeak;
        }

        public TalkVoiceMode(): this(true,true){ }

        public TalkVoiceMode(int value)
        {
            switch (value)
            {
                case 1:
                    EnableListen = true;
                    EnableSpeak = true;
                    break;
                case 2:
                    EnableListen = true;
                    EnableSpeak = false;
                    break;
                case 3:
                    EnableListen = false;
                    EnableSpeak = true;
                    break;
                case 4:
                    EnableListen = false;
                    EnableSpeak = false;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("value");
            }
            
        }

        public static implicit operator TalkVoiceMode(int value)
        {
            return new TalkVoiceMode(value);
        }

        public static implicit operator int(TalkVoiceMode value)
        {
            return value.ToInt();
        }

        public int ToInt()
        {
            if (EnableListen && EnableSpeak)
                return 1;
            else if (EnableListen && !EnableSpeak)
                return 2;
            else if (!EnableListen && EnableSpeak)
                return 3;
            else
                return 4;
        }

        public bool EnableListen;
        public bool EnableSpeak;
    }
}