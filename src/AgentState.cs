﻿using System;

namespace hesong.plum.client
{
    public enum AgentState
    {
        None = 0,
        Logged = 1,
        Online = 2,
        Idle = 3,
        Away = 4,
        Distributed = 5,
        Connecting = 6,
        Connected = 7,
        PostConnected = 8,
        AcdInit = 9
    }
}