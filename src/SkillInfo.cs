using Newtonsoft.Json;

namespace hesong.plum.client
{
    public class SkillInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("dn")]
        public string DisplayName { get; set; }

        [JsonProperty("checked")]
        public bool Checked { get; set; }

        [JsonProperty("belonged")]
        public bool Belonged{ get; set; }
    }
}
