using System.Collections.Generic;

using Newtonsoft.Json;

namespace hesong.plum.client
{
    public class AcdInfo
    {
        [JsonProperty("fiid")]
        public string Id { get; set; }

        [JsonProperty("workspace_name")]
        public string WorkspaceName { get; set; }

        [JsonProperty("create_time")]
        public double CreateTime { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("state")]
        public AcdState State { get; set; }

        [JsonProperty("initial_call")]
        public CallInfo InitialCall { get; set; }

        [JsonProperty("talkers")]
        public List<TalkerInfo> Talkers { get; set; }

        [JsonProperty("options")]
        public AcdOptions Options { get; set; }

        [JsonProperty("initial_agent_name")]
        public string InitialAgentName { get; set; }

        [JsonProperty("target_agent_name")]
        public string TargetAgentName { get; set; }

        [JsonProperty("target_line_telnum")]
        public string TargetLineTelnum { get; set; }

        [JsonProperty("tag")]
        public object Tag { get; set; }
    }
}