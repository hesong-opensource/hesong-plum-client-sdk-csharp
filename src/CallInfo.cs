using Newtonsoft.Json;

namespace hesong.plum.client
{
    public class CallInfo
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("process_id")]
        public string ProcessId { get; set; }

        [JsonProperty("to")]
        public string To { get; set; }

        [JsonProperty("from")]
        public string From { get; set; }

        [JsonProperty("chan")]
        public int Chan { get; set; }

        [JsonProperty("dir")]
        public int Dir { get; set; }
    }
}