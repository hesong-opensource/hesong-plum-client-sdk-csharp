namespace hesong.plum.client
{
    public enum CallAgentState
    {
        Initial = 0,
        Started = 1,
        Ringing = 2,
        Connected = 3,
        Disconnected = 4,
        Failed = -1
    }
}
